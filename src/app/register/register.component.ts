import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  authenticationService: any;
  returnUrl: any;
  alertService: any;
  model: any = {};
  loading = false;
  Parse:any;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  register() {
    var vm = this;
        var user = new Parse.User();
        user.set("username", this.model.username);
        user.set("password", this.model.password);
        user.set("email", this.model.email);
        user.set("FullName", this.model.fullname);
        user.set("Role", 'User');

        // user.signUp(null,{
        //   success: function(user) {
        //     alert("Signup Successfull");
        //     vm.router.navigate(['/login'])
        //   },
        //   error: function(user, error) {
        //    alert("Error: " + error.code + " " + error.message);
        //   alert("Signup Failed");
        // }
        // });
  }

}
