import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  authenticationService: any;
  returnUrl: any;
  alertService: any;
  model: any = {};
  loading = false;
  Parse:any;
  currentUser:any;
  isLogged=false;
  constructor(private router: Router) { }

  ngOnInit() {
  }

  login() {
    var vm = this;
    Parse.User.logIn(this.model.username,this.model.password, {
      success: function(user) {
        this.isLogged=true;
        vm.currentUser = Parse.User.current();
        localStorage.setItem('currentUser', JSON.stringify(vm.currentUser));
        if(user.attributes.Role==='Admin') {
          window.location.href='/adminHome';
        } else {
          window.location.href='/userHome';
        }
       //window.alert('success');

      },
      error: function(user, error) {
        window.alert('Login Failed');
        // The login failed. Check error to see why.
      }
    });
}
register(){
  this.router.navigate(['/register'])
};

}
