import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {
  totalScore:any;
  constructor(private router:Router) { }

  ngOnInit() {
    this.getScore();
  }

  getScore()
  {
    var vm = this;
    var User = Parse.Object.extend("User");
    var query = new Parse.Query(User);
    query.equalTo("Role", 'User');
    query.select("username","TotalScore","FullName");
    query.find().then(function (results) {
      vm.totalScore =results;
    });
  }
  addQues(){
    this.router.navigate(['/adminadd'])
  }
  // logout(){
  //   localStorage.removeItem('currentUser');
  //   this.router.navigate(['/login']);
  // }

}
