import { Component, OnInit } from '@angular/core';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';
@Component({
  selector: 'app-admin-view',
  templateUrl: './admin-view.component.html',
  styleUrls: ['./admin-view.component.css']
})
export class AdminViewComponent implements OnInit {
  editService: any;
  gridState: any;
  editedProduct: any;
  editedRowIndex: any;
  ProductList = [];
  dataItem: any;
  constructor() { }

  ngOnInit() {
    this.getQA();
  }

  getQA() {
    this.ProductList = [];
    var vm = this;
    var QA = Parse.Object.extend("AdminModel");
    var query = new Parse.Query(QA);
    query.select("modelQuestion","modelAnswer");
    query.find({
      success: function(result:any[]) {
        result.forEach((element) => {
          var elem = element.attributes;
          vm.ProductList.push({
            modelQuestion: elem.modelQuestion,
            modelAnswer: elem.modelAnswer,
            id:element.id
          });
        });
      },
      error: function(error) {
        alert("Error: " + error.code + " " + error.message);
      }
    });
  }

  public editHandler({sender, rowIndex, dataItem}) {
    this.closeEditor(sender);

    this.editedRowIndex = rowIndex;
    this.editedProduct = Object.assign({}, dataItem);

    sender.editRow(rowIndex);
}
private closeEditor(grid, rowIndex = this.editedRowIndex) {
  grid.closeRow(rowIndex);
 // this.editService.resetItem(this.editedProduct);
  this.editedRowIndex = undefined;
  this.editedProduct = undefined;
}
public cancelHandler({sender, rowIndex}) {
  this.closeEditor(sender, rowIndex);
}
public onStateChange(state: State) {
  this.gridState = state;

  this.editService.read();
}
public saveHandler({sender, rowIndex, dataItem, isNew}) {
  var AdminModel = Parse.Object.extend("AdminModel");
      var query = new Parse.Query(AdminModel);
      query.equalTo("objectId", dataItem.id);
      query.first({
        success: function (object) {
          object.set("modelQuestion", dataItem.modelQuestion);
          object.set("modelAnswer", dataItem.modelAnswer );
          object.save();
        },
        error: function (error) {
          alert("Error: " + error.code + " " + error.message);
        }
      });

  sender.closeRow(rowIndex);

  this.editedRowIndex = undefined;
  this.editedProduct = undefined;
}

public removeHandler({dataItem}) {
  var vm =this;
  var AdminModel = Parse.Object.extend("AdminModel");
  var query = new Parse.Query(AdminModel);
  query.equalTo("objectId", dataItem.id);
  query.first({
    success: function (object) {
      object.destroy({
        success: function(myObject) {
          vm.getQA();
        },
        error: function(myObject, error) {
          // The delete failed.
          // error is a Parse.Error with an error code and message.
        }
      });
    },
    error: function (error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });
}


}
