import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-add',
  templateUrl: './admin-add.component.html',
  styleUrls: ['./admin-add.component.css']
})
export class AdminAddComponent implements OnInit {

  authenticationService: any;
  returnUrl: any;
  alertService: any;
  model: any = {};
  loading = false;
  Parse:any;
  constructor(private router:Router) { }

  ngOnInit() {
  }
  addModelQA(){
    var vm = this;
var AdminModel = Parse.Object.extend("AdminModel");
var adminModel = new AdminModel();

adminModel.set("modelQuestion",this.model.Question);
adminModel.set("modelAnswer", this.model.Answer);
//adminModel.set("id", false);

adminModel.save(null, {
  success: function(adminModel) {
    // Execute any logic that should take place after the object is saved.
    alert('Model Q&A Added Successfully');
    window.location.href='/adminview';
  },
  error: function(adminModel, error) {
    alert('Failed to create new object, with error code: ' + error.message);
    // Execute any logic that should take place if the save fails.
    // error is a Parse.Error with an error code and message.
    alert('Adding Model Q&A Failed');
  }
});
  }

  // logout(){
  //   localStorage.removeItem('currentUser');
  //   this.router.navigate(['/login']);
  // }
}
