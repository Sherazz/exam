import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AdminHomeComponent } from './admin/admin-home/admin-home.component';
import { UserHomeComponent } from './user/user-home/user-home.component';
import { RegisterComponent } from './register/register.component';
import { AdminAddComponent } from './admin/admin-add/admin-add.component';
import { UserexamComponent } from './user/userexam/userexam.component';
import { AdminViewComponent } from './admin/admin-view/admin-view.component';
import { HomeComponent } from './home/home.component';
const routes: Routes = [
  // { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },{ path: 'adminHome', component: AdminHomeComponent },
  { path: 'userHome', component: UserHomeComponent },{ path: 'register', component: RegisterComponent },
  { path: 'adminadd', component: AdminAddComponent },{ path: 'userexam', component: UserexamComponent },
  { path: 'adminview', component: AdminViewComponent },{ path: '', component: HomeComponent, pathMatch: 'full' }
];
@NgModule({
  imports: [
    CommonModule,RouterModule.forRoot(routes)
  ],
  exports:[RouterModule],
  declarations: []
})


export class AppRoutingModule {
  
 }
