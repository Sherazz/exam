import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.css']
})
export class UserHomeComponent implements OnInit {
  id: any;
  result:any;
  currentUser: any;
  splitIndividual:any;
  totalScore:any;
  splitI:any;
  constructor(private router:Router) { }

  ngOnInit() {
    this.getScore();
  }

  getScore()
  {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    //this.id=this.currentUser.objectId;
    var vm = this;
    var User = Parse.Object.extend("User");
    var query = new Parse.Query(User);
    query.equalTo("objectId", this.currentUser.objectId);
    query.select("IndividualScore","TotalScore");
    query.find().then(function (results) {
      vm.result = results[0].attributes.IndividualScore;
      vm.splitIndividual = vm.result.split('+');
      vm.splitIndividual.pop();
      vm.totalScore = results[0].attributes.TotalScore;
    });
  }

  // logout(){
  //   localStorage.removeItem('currentUser');
  //   this.router.navigate(['/login']);
  // }
}
