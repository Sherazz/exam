import { Component, OnInit } from '@angular/core';
import { HttpClient } from 'selenium-webdriver/http';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Rx";
import 'rxjs/Rx';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-userexam',
  templateUrl: './userexam.component.html',
  styleUrls: ['./userexam.component.css']
})
export class UserexamComponent implements OnInit {
  currentUser: any;
  total: number=0;
  score: string='';
  constructor(private http: Http,private router: Router,private spinner: NgxSpinnerService) { }
  result: any;
  adminAnswer: any;
  j:number=0;
  ngOnInit() {
    this.getQuestions();
  }

  getQuestions() {
    var vm = this;
    var AdminModel = Parse.Object.extend("AdminModel");
    var query = new Parse.Query(AdminModel);
    query.select("modelQuestion","modelAnswer");
    query.find().then(function (results) {
      vm.result = results;

    });

  }

  
  submitExam(j) {
    var vm = this;
    var req = false;
    var count = this.result.length;
    if(j===0) {
    for(var i=0;i<count;i++) {
      var user_answer = (<HTMLInputElement>document.getElementById(this.result[i].id)).value;
      if(user_answer==='') {
        (<HTMLInputElement>document.getElementById(this.result[i].id+'+req')).innerHTML='Please Enter an Answer';
        req = true;
      }
      else {
        (<HTMLInputElement>document.getElementById(this.result[i].id+'+req')).innerHTML='';
        req = false;
      }
    }
  }
    if(!req) {
    if(j<count) { 
      var user_answer = (<HTMLInputElement>document.getElementById(this.result[j].id)).value;
      var requestJson = '{' + '"id":' + j + ',"admin_answer":"' + this.result[j].attributes.modelAnswer + '","user_answer":"' + user_answer + '"}';
      //let Url = 'https://cors-anywhere.herokuapp.com/http://answer-score.herokuapp.com';
        let Url = 'http://ec2-54-218-97-229.us-west-2.compute.amazonaws.com';
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let body = requestJson;
        vm.http.post(Url, body, options).map(res => res.json())
            .subscribe(
                (data) => {
                this.score =this.score + data.score + '+';
                this.total =this.total + data.score;
                },
                (err) => console.log(err));
    }
    else
    {
      setTimeout(function(){ 
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      var Users = Parse.Object.extend("User");
      var query = new Parse.Query(Users);
      query.equalTo("objectId", this.currentUser.objectId);
      query.first({
        success: function (object) {
          object.set("IndividualScore", vm.score);
          object.set("TotalScore", vm.total);
          object.save();
          vm.router.navigate(['/userHome']);
        },
        error: function (error) {
          alert("Error: " + error.code + " " + error.message);
        }
      });
    }, 200000);
      return;
    
}
    setTimeout(function(){ 
      vm.submitExam(j+1);
    }, 5000);
  }
}

  }